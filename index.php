<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="js/jquery.js" type="text/javascript"></script>
        <script src="js/monJsdemo1.js" type="text/javascript"></script>
    </head>
    <body>

<?php

  require_once ("include/class.pdo.inc.php");

  $pdo = PdoEtablissement::getPdoEtablissement();
  $lesEtablissements = $pdo->getLesEtablissements();
  $lesArrondissements = $pdo->getLesArrondissements();
 
?>
        <h2>Choisir votre école primaire</h2>

        Sélectionner un arrondissement
        <select id="arrondissements" name ="arrondissements">
          <?php
          echo '<option disabled selected>... </option>'; 
          foreach($lesArrondissements as $unArrondissement){
              echo '<option> '. $unArrondissement['arrondissement'].' </option>';
          }
          ?>  
            
        </select>
        Sélectionner une école de cet arrondissement

        <select id="ecoles" name="ecoles">
          <?php
          echo '<option disabled selected>... </option>'; 
          foreach($lesEtablissements as $unEtablissement){
              echo '<option> '. $unEtablissement['nom'].' </option>';
          }
          ?>
        </select>
        
        <div id ="detail" name="detail"> 

        </div>
    </body>
</html>