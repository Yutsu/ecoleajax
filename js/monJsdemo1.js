$(function(){
    
    $("#section1").on("click",
        function(){
            $("#section1").load("import.html");
        }
    );
    
    $("#envoyer").on("click",
        function(){
            $("#sectionnom").load("import1bis.php", {"nom" : $("#nom").val()} );
        }
    );
    
    $("input[name='plat']").on("click",
        function(){
            $("#pied").load("import1ter.php", {"plat" : $(this).val()} );
        }
    );
    
    $("select[name='table']").on("click", //lorsqu'une table est sélectionnée
    //function(){
    //    $("#pied").load("import1quart.php", {"table" : $(this).val()} );
    //}
        
    function(){
        $.get("import1quart.php", {"table" : $(this).val() },
        //function(data){alert(data)}); 
        function(data){$("#pied").html(data)});
       
    });
    
    
    //Demo 3
    $("#bouton").on("click",
        function(){
            $.post("retourPost1.php", {"nom" : $("#nom").val(),
                                        "prenom" : $("#prenom").val(),
                                        "adresse" : $("#adresse").val(),
                                        "tel" : $("#tel").val()},
                                         foncRetour); });
        function foncRetour(data){
           $("#pied").html(data);
        }
        
        
    //Demo 4
    $("select[name='table']").on("change",
        function(){
           $.get("retourJson.php", {"table" : $(this).val() },
                foncRetourne, "json");
        });
        
        function foncRetourne(data){
            $("#tab").empty(); // pour vider la liste
            for(var key in data)
                $("#tab").append("<tr><td>" + key + "</td><td>" + data[key] + "</td></tr>");
        }
        
        
    //Index
   $("select[name='arrondissements']").on("change",
        function() {
            $.get("retourJsonArron.php",{"arrondissement" : $(this).val() },
                foncRetour, "json");
        });

    function foncRetour(data) {
        $("#ecoles").empty();
                $("#detail").empty();

        $("#ecoles").append("<option disabled selected>Choisir...</option>");
        for (var key in data) {
            $("#ecoles").append("<option value="+data[key].code+">" + data[key].genre + " " + data[key].nom + "</option>");
        }
    }

    //Index détail des établissements
    $("select[name='ecoles']").on("change",
        function() {
            $.get("retourJsonEtab.php",{"code" : $(this).val() },
                foncRetour2, "json");
        });

    function foncRetour2(data) {
        $("#detail").empty();
        for (var key in data) {
            $("#detail").append("<p> Nom : " + data[key].nom + 
                                "<br> Genre : " + data[key].genre +
                                "<br> Adresse : " + data[key].adresse +
                                "<br> Arrondissement : " + data[key].arrondissement + "</p>");
        }
    }
  
    
});